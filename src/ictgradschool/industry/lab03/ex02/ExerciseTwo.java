package ictgradschool.industry.lab03.ex02;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    public static void start() {
        System.out.print("Please input lower bound");
        int lower = Integer.parseInt(ictgradschool.Keyboard.readInput());
        System.out.print("Please input upper bound");
        int upper = Integer.parseInt(ictgradschool.Keyboard.readInput());

        int random1 = (int)(lower + Math.random()* ((upper - lower) + 1));
        int random2 = (int)(lower + Math.random()* ((upper - lower) + 1));
        int random3 = (int)(lower + Math.random()* ((upper - lower) + 1));

        int smallestRandom = Math.min(random1, Math.min(random2, random3));

        System.out.println(random1 + " and " + random2 + " and " + random3);

        System.out.println("Smallest Number is " + smallestRandom);

    }

    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();



    }
}

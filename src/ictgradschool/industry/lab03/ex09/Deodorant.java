package ictgradschool.industry.lab03.ex09;

public class Deodorant {

    private String brand;
    private String fragrance;
    private boolean rollOn;
    private double price;

    public Deodorant(String brand, String fragrance,
                     boolean rollOn, double price) {

        this.brand = brand;
        this.fragrance = fragrance;
        this.rollOn = rollOn;
        this.price = price;
    }

    public String toString() {
        String info = brand + " " + fragrance;
        if (rollOn) {
            info = info + " Roll-On";
        } else {
            info = info + " Spray";
        }
        info +=  " Deodorant, \n$" + price;
        return info;
    }


    public double getPrice() {

        return price;
    }

    public String getBrand() {

        return brand;
    }

    public boolean isRollOn() {

        return rollOn;
    }

    public String getFragrance() {

        return fragrance;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setBrand(String brand) {
        this.brand = brand;

    }

    public void setFragrance(String fragrance) {
        this.fragrance = fragrance;
    }

    public boolean isMoreExpensiveThan(Deodorant other) {
     if(this.price > other.getPrice()) {
         return true;
     } else {
         return false;
     }

    }

    public static void main(String[] args) {
        Deodorant myDeodorant = new Deodorant("Gentle", "Baby Powder", true, 4.99);

        Deodorant yourDeodorant = new Deodorant("Spring", "Blossom", false, 3.99);

        if (yourDeodorant.isMoreExpensiveThan(myDeodorant)) {

            System.out.println("5. Most expensive is " + yourDeodorant.getBrand());
        } else {

            System.out.println("5. Most expensive is " + myDeodorant.getBrand());
        }


    }

}
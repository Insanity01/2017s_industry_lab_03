package ictgradschool.industry.lab03.ex03;

/**
 * Write a program that prompts the user to enter a sentence, then prints out the sentence with a random character
 * missing. The program is to be written so that each task is in a separate method. See the comments below for the
 * different methods you have to write.
 */
public class ExerciseThree {

    public void start() {

        String sentence = getSentenceFromUser(); // returns the input as sentence
        int randomPosition = getRandomPosition(sentence); // gets a random position based on the sentence
        printCharacterToBeRemoved(sentence, randomPosition);
        String changedSentence = removeCharacter(sentence, randomPosition);
        printNewSentence(changedSentence);
    }

    public String getSentenceFromUser() {

        System.out.println("Pleas enter sentence"); // prompt
        return ictgradschool.Keyboard.readInput(); //returns the input
    }

    public int getRandomPosition(String sentence) {

        int sentenceLength = sentence.length() - 1; // max length of string
        return (int) (Math.random() * sentenceLength + 1); //generates a random position from 0 to length of string -1 an returns
    }

    public  void printCharacterToBeRemoved(String sentence, int randomPosition) {

        System.out.println("Character to be removed is " + sentence.charAt(randomPosition));

    }

    public String removeCharacter(String sentence, int randomPosition) {

        String newSentence = "";

        for (int i = 0; i <= (sentence.length()-1); i++){
            if (i != randomPosition) {
                newSentence = newSentence + sentence.charAt(i);
            } else {
                continue;
            }
        }

        return newSentence;

    }

    public void printNewSentence(String changedSentence) {
        System.out.println(changedSentence);
    }


    public static void main(String[] args) {
        ExerciseThree ex = new ExerciseThree();
        ex.start();
    }
}

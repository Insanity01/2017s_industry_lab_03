public class ExerciseEight {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    public static void start() {

        double radius;

        System.out.println("\"Volume of a Sphere\"");

        System.out.println("Enter the radius: ");

        radius = Double.parseDouble(ictgradschool.Keyboard.readInput());

        double volume = (4 * Math.PI * Math.pow(radius, 3))/3;

        System.out.println("Volume: " + volume);

    }

    public static void main(String[] args) {

        ExerciseEight ex = new ExerciseEight();
        ex.start();


    }
}
